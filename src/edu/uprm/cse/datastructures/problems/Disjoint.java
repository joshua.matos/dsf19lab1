package edu.uprm.cse.datastructures.problems;

import javax.naming.spi.DirStateFactory.Result;

import edu.uprm.cse.datastructures.set.ArraySet;
import edu.uprm.cse.datastructures.set.Set;

public class Disjoint {
	
	private static boolean checkDisjoint(Object[] a2) 
	{
		Object[] A1 = new Object[10];
		Object[] A2 = new Object[10];
		for(int i=0;i<a2.length-1;++i) 
		{
			Set<Integer>S1 = (Set<Integer>)a2[i];
			A1 = S1.toArray();
			Set<Integer>S2 = (Set<Integer>)a2[i+1];
			A2 = S2.toArray();
			for(int count=0; count<A1.length;++count)
			{
				for(int counter=0;counter<A2.length;++counter)
				{
					if(A1[count]==A2[counter]) 
					{
						return false;
					}				
				}
			}
		}
		return true;
	}
	
	public static void main(String[] args) {
		Set<Integer> S1 = new  ArraySet<Integer>();
		Set<Integer> S2 = new  ArraySet<Integer>();
		Set<Integer> S3 = new  ArraySet<Integer>();
		Set<Integer> S4 = new  ArraySet<Integer>();
		
		S1.add(1);
		S1.add(2);
		
		S2.add(0);
		
		S3.add(5);
		S3.add(8);
		S3.add(9);
		
		S4.add(5);

		@SuppressWarnings("unchecked")
		Object[] A1 =   new Object[3];
		A1[0] = S1;
		A1[1] = S2;
		A1[2] = S3;

		System.out.println("S1, S2, and S3 disjoint: " + checkDisjoint(A1));
		Object[] A2 =   new Object[4];
		A2[0] = S1;
		A2[1] = S2;
		A2[2] = S3;
		A2[3] = S4;

		System.out.println("S1, S2, S3, and S4 disjoint: " + checkDisjoint(A2));


	}


}
